import React from 'react';
import { BrowserRouter, Route } from "react-router-dom"
import Title from './components/Title.js'
import BookList from './components/BookList.js'
import FavList from './components/FavList.js'
import './styles/MainContent.css'
import Header from './components/Header.js'
import Footer from './components/Footer.js'
//import FavoriteBooks from './components/FavCard.js'

class App extends React.Component{
 render(){
    return(
      <BrowserRouter>
        <Header/>
        <Route path ="/" exact>
          <React.Fragment>
            <div>
              <Title/>
            <div className ="container">
              <BookList/>
              <FavList/>
            </div>
            </div>
          </React.Fragment>
        </Route>
        <Route path = "/about" exact>
          test
        </Route>
        <Route path = "/faq" component={ Faq } exact/>
        <Footer/>
      </BrowserRouter>
    )
  }
}

export default App;

const About = () => {
  return <div>about</div>
}

const Faq = () => {
  return <div>faq</div>
}

