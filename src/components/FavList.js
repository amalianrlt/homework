import React, { Component } from "react";
import "../styles/MainContent.css";
import FavCard from './FavCard.js'

class FavList extends Component{
  state = {
    favorites:[]
  }

addToFavorite = id => {
    for(let i = 0; i < this.state.favorites.length; i++){
      if(this.state.favorites[i].id === id) {
        return false
      }
    }
    const data = this.state.books.find(item => item.id === id);
    this.setState({
      favorites: [...this.state.favorites.data]
    })
  }

  removeFromFavorite = id => {
    this.setState({
      favorites: this.state.favorites.filter(item => item.id !== id)
    })
  } 



render(){  
    return(
      <div>
        <FavCard favorites={ this.state.favorites } remove={this.removeFromFavorite} />
      </div>
    )
}
}


export default FavList;
