import React, { Component } from "react";
import BookCard from './BookCard';
import "../styles/MainContent.css";
import FavoriteBooks from './FavCard.js'

class BookList extends Component{
  state = {
    books:[
      {
        id : 1,
        title :"Buku Aktivitas Anak Shaleh : Kegiatan",
        author : "Yeti Nurmayanti",
        price : "Rp. 57.000",
        image : require ("../img/1.jpg")
      },
      {
        id : 2,
        title :"Komik Kkpk : Festival Unicorn",
        author : "Naura Nuraqila, Dkk",
        price : "Rp. 35.000",
        image : require ("../img/2.jpg")
      },
      {
        id : 3,
        title : "Tanya Jawab Seru Tentang Lautan",
        author : "Miles Kelly",
        price : "Rp. 45.000",
        image : require ("../img/3.jpg")
      },
      {
         id : 4,
        title :"Komik Next G Jujur Itu Penting",
        author : "Wyda Khosyighina Maitsa, dkk.",
        price : "Rp. 36.000",
        image : require ("../img/4.jpg")
      }
    ]

  }
  
  render(){
    return(
      <div>
        <BookCard className="books"books={ this.state.books } add={this.addToFavorite}/>
      </div>
    )
  }
}

export default BookList;

