import React from "react";
import "../styles/MainContent.css"
import AddButton from './AddButton'

 const BookCard = props => {
  const book = props.books.map(item =>
    <div className="book-card" key={item.id}>
      <img className="img-book" src= { item.image } alt={ item.image } />
      <h4 className ="title-book">{item.title}</h4>
      <p className ="author-book" >{item.author}</p>
      <h5 clasName ="price-book">{ item.price }</h5>
    <button onClick={() => props.add(item.id)}>add to favorite</button> 
  </div>
)
  console.log(props)
  return(
    <div className="book-container">
      { book }
    </div>
  )
}


export default BookCard;

